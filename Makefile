CC = tcc

gen.exe: gen.c
	$(CC) -o gen.exe gen.c

odd_or_even.c: gen.exe
	powershell "./gen.exe 4 | Out-File -Encoding Ascii odd_or_even.c # example value"

odd_or_even.exe: odd_or_even.c
	$(CC) -o "odd_or_even.exe" odd_or_even.c

.PHONY: run

example: odd_or_even.exe
	./odd_or_even.exe 5
