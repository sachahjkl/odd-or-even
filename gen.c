#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

void crash(const char * message);
static char * program_name;
int main(int argc, char **argv){
	program_name = argv[0];
	
	if(argc <=1) {
		crash("not enough parameters.");
	}

	uint32_t power = atoi(argv[1]);

	if(power == 0) {
		crash("invalid value.");
	}

	printf("/* Copyright 2023. All unauthorized distribution of this source code\n");
	printf("   will be persecuted to the fullest extent of the law*/\n");

	printf("#include <stdio.h>\n");
	printf("#include <stdint.h>\n");
	printf("#include <stdlib.h>\n");

	printf("int main(int argc, char* argv[])\n");
	printf("{\n");
	printf("    uint32_t number = atoi(argv[1]); // No problems here\n");
	printf("// -------> MAX VALUE IS = %llu\n", 1LLU << power);
	for(unsigned long long i = 0; i < (1LLU << power); i++){
		printf("    if (number == %llu)\n", i);
		if (i % 2 == 0)
			printf("	printf(\"even\\n\");\n");
		else
			printf("	printf(\"odd\\n\");\n");
	}

	printf("}\n");
	return 0;
}

void crash(const char * message){
		printf("%s\n",message);
		printf("Usage: %s <exponent>\n", program_name);
		printf("\texponent: the exponent to use for the max value of our program\n");
		exit(1);
}
