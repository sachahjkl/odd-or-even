# Odd or even program generator

Small meta-program that generates the most stupid C code possible capable of telling you wether a given integer is odd or even.

## Usage in powershell

```powershell
# Build the generator program
make gen.exe

# Call the generator program with the example value of "16"
.\gen.exe 16 | Out-File -Encoding ascii .\odd_or_even.c

# Build the odd_or_even program
make odd_or_even.exe


# Call the odd_or_even program
.\odd_or_even.exe 15
odd
```
